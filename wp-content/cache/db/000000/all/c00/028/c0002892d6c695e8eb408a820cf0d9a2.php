�T<?php exit; ?>a:6:{s:10:"last_error";s:0:"";s:10:"last_query";s:191:"SELECT   wp_posts.* FROM wp_posts  WHERE 1=1  AND wp_posts.ID IN (8,10,12,14,16,48) AND wp_posts.post_type = 'page' AND ((wp_posts.post_status = 'publish'))  ORDER BY wp_posts.post_date DESC ";s:11:"last_result";a:6:{i:0;O:8:"stdClass":23:{s:2:"ID";s:2:"48";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-08-19 05:02:46";s:13:"post_date_gmt";s:19:"2014-08-19 05:02:46";s:12:"post_content";s:932:"<h3>RECRUITING, SELECTING AND TRAINING YOUR FSED STAFF.</h3>
In a small, tightly run ER, lives are at stake by definition, and so every person’s medical and professional competency is crucial, of course. But other traits are important as well, including the ability to work with other staff, interact well with patients, and so on. Finding the right person for each position is a special skill unto itself. At REMCO, we follow procedures to:
<ul>
	<li>Establish selection criteria for each position (physician, nurse, lab tech, etc.).</li>
	<li>Recruit from known professional sources.</li>
	<li>Acquire and validate accuracy of resumes, recommendations.</li>
	<li>Verify education, specialties/certifications, current licensing, hospital privileges, and more.</li>
	<li>Screen and interview all applicants under consideration.</li>
	<li>Make new hires, orient and provide training for all employees as needed.</li>
</ul>";s:10:"post_title";s:8:"Staffing";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:30:"recruitment-selection-training";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-09-18 12:07:56";s:17:"post_modified_gmt";s:19:"2014-09-18 12:07:56";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:35:"http://kannan.remco.dev/?page_id=48";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:1;O:8:"stdClass":23:{s:2:"ID";s:2:"16";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-08-19 04:53:27";s:13:"post_date_gmt";s:19:"2014-08-19 04:53:27";s:12:"post_content";s:866:"<h3>MAKING YOUR FSED COMPLIANT WITH GOVERNMENT REGULATIONS.</h3>
To legally operate, your FSED must be fully compliant with regulations established by government at various levels. Federal requirements regarding payroll and benefits are covered under the section dealing with FSED management and operation. Many aspects of medical operation and licensing is dealt with at the state level. In all cases, our expertise in dealing with these requirements will provide the knowledge necessary to assure timely filing of forms, license acquisition or renewal, and more. Specific examples include:
<ul>
	<li>State FSED Licensing</li>
	<li>Approved State Transfer Agreement, MOT, Transfer Policy</li>
	<li>Compliance with Title 25 of Texas Administrative Code Chapter 131</li>
	<li>Registration, certification, licensing for equipment, lab, pharmacy, etc.</li>
</ul>";s:10:"post_title";s:26:"Govt. Regulation Expertise";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:25:"govt-regulation-expertise";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-09-18 12:07:03";s:17:"post_modified_gmt";s:19:"2014-09-18 12:07:03";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:35:"http://kannan.remco.dev/?page_id=16";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:2;O:8:"stdClass":23:{s:2:"ID";s:2:"14";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-08-19 04:52:39";s:13:"post_date_gmt";s:19:"2014-08-19 04:52:39";s:12:"post_content";s:1598:"<h3>MANAGING AND OPERATING
YOUR FSED.</h3>
Every FSED is a medical facility, with special licensing and operational requirements – such as radiology, lab, and pharmacy components. Yet, at the same time, it has all the issues that confront any business office – from computers to coffeemakers, from medical coding to committees, from payroll to everyday paperwork. We provide the support needed to make sure that all aspects run smoothly and seamlessly.
<ul>
	<li>Appropriate license acquisition for each special operational entity.</li>
	<li>Laboratory:
<ul>
	<li>Equipment acquisition and set-up</li>
	<li>CLIA/COLA Certification</li>
	<li>API Testing/Registry, Policies and Procedures</li>
</ul>
</li>
	<li>Pharmacy:
<ul>
	<li>Texas State Board Of Pharmacy Class F Licensing</li>
	<li>Pharmacist</li>
	<li>Pharmacy Tech Support</li>
	<li>Acquisition and stocking of essential pharmaceutical inventory</li>
</ul>
</li>
	<li>Radiology Suite
<ul>
	<li>Equipment acquisition and set-up</li>
	<li>Radiation Safety Users Registration</li>
	<li>RSO</li>
</ul>
</li>
	<li>Pre-State Conference Support</li>
	<li>Mock Site Survey</li>
	<li>DSHS Survey Expertise</li>
	<li>Board Governance</li>
	<li>Establishment of State Required Committees</li>
	<li>Implementation of State Approved Policy and Procedure Manuals</li>
	<li>Front Office-Back Office Support</li>
	<li>Payroll Services</li>
	<li>Medical Coding and Billing</li>
	<li>Ongoing Management Support</li>
	<li>Vendor Selection and Support</li>
	<li>Equipment Selection</li>
	<li>IT Support-EMR</li>
</ul>";s:10:"post_title";s:32:"Management & Operational Support";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:30:"management-operational-support";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-09-18 01:17:05";s:17:"post_modified_gmt";s:19:"2014-09-18 01:17:05";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:35:"http://kannan.remco.dev/?page_id=14";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:3;O:8:"stdClass":23:{s:2:"ID";s:2:"12";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-08-19 04:52:02";s:13:"post_date_gmt";s:19:"2014-08-19 04:52:02";s:12:"post_content";s:972:"<h3>BUILDING YOUR FSED.</h3>
Unless you’ve overseen commercial construction in the past, this is a phase of development in which you’ll want to have trusted and experienced advisors. Some of the issues to be dealt with include:
<ul>
	<li>Build &amp; finance options: custom build, build-to-suit with lease-back, other.</li>
	<li>Alternative approaches – lease/buy &amp; refit existing building; lease appropriate office or retail space and finish out.</li>
	<li>Choice of commercial contractor – preferably one with extensive medical building experience.</li>
	<li>Architectural plans suitable for the ER facility.</li>
	<li>Accommodation for structural, electrical, HVAC and other features appropriate for your FSED.</li>
	<li>Acquisition of specialized medical equipment – decision to buy or lease.</li>
	<li>Plan for timely and proper installation of large and/or sensitive equipment.</li>
	<li>Project a realistic construction schedule.</li>
</ul>";s:10:"post_title";s:12:"Construction";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:12:"construction";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-09-18 12:03:59";s:17:"post_modified_gmt";s:19:"2014-09-18 12:03:59";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:35:"http://kannan.remco.dev/?page_id=12";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:4;O:8:"stdClass":23:{s:2:"ID";s:2:"10";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-08-19 04:51:38";s:13:"post_date_gmt";s:19:"2014-08-19 04:51:38";s:12:"post_content";s:1001:"<h3>SELECTING A SITE FOR YOUR FSED.</h3>
The location of a freestanding emergency department can be critical to its perceived value to the community it serves – and thus to its initial and ongoing success. A great deal can go into the site selection process, including:
<ul>
	<li>Geographic survey – general study of potential communities to be considered.</li>
	<li>Demographic profiling – neighborhood property values, resident income levels, etc.</li>
	<li>Economic viability and financial analysis – to gauge likely success of FSED operation.</li>
	<li>Client preference – where do you want your FSED?</li>
	<li>Real estate availability – what appropriately zoned commercial land is available?</li>
	<li>Real estate cost – what are commercial real estate costs in your selected market?</li>
	<li>Real estate acquisition – land lease or purchase?</li>
	<li>Alternative approaches – lease/buy &amp; refit existing building; appropriate lease space/finish-out.</li>
</ul>";s:10:"post_title";s:14:"Site Selection";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"site-selection";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-09-18 12:03:34";s:17:"post_modified_gmt";s:19:"2014-09-18 12:03:34";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:35:"http://kannan.remco.dev/?page_id=10";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}i:5;O:8:"stdClass":23:{s:2:"ID";s:1:"8";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2014-08-19 04:49:57";s:13:"post_date_gmt";s:19:"2014-08-19 04:49:57";s:12:"post_content";s:2303:"<h3>COUNTLESS DECISIONS – OR ONE.</h3>
<h4><span style="color: red;">A TURNKEY SOLUTION TO FSED DEVELOPMENT.</span></h4>
As important and as profitable as freestanding emergency departments can be, the move to begin development is no small decision. And it takes no small amount of effort and expertise to set things up the right way.

But that’s one of the very reasons why we established REMCO Emergency, LLC in the first place. Our founding partners collectively possess over 100 years of clinical, business, financial, operational and management experience. And we have particular knowledge and understanding of FSED projects.

So if your goal is to develop one or more FSEDs on your own, we’ll be happy to help in any individual area of expertise that you need. But we want to emphasize that you also have a much simpler, single-decision option: hire us to provide a turnkey solution – one that can assure a network of immediate and ongoing support, as well as professional execution of all specialized activities listed below.
<ul>
	<li>Site Selection</li>
	<li>Demographic profiling, economic viability and financial analysis for Client Selected Site</li>
	<li>Construction</li>
	<li>Vendor Selection and Support</li>
	<li>Equipment Selection</li>
	<li>IT Support-EMR</li>
	<li>State FSED Licensing</li>
	<li>Approved State Transfer Agreement, MOT, Transfer Policy</li>
	<li>Recruitment, Selection and Training of Employees</li>
	<li>Management and Operational Support</li>
	<li>Governmental Regulation Expertise</li>
	<li>Compliance with Title 25 of Texas Administrative Code Chapter 131</li>
	<li>Lab CLIA/COLA Certification, API Testing/Registry, Policies and Procedures</li>
	<li>Pharmacy Texas State Board Of Pharmacy Class F Licensing, Pharmacist, Pharmacy Tech Support</li>
	<li>Radiology Suite Set Up, Radiation Safety Users Registration, RSO</li>
	<li>Pre-State Conference Support</li>
	<li>Mock Site Survey</li>
	<li>DSHS Survey Expertise</li>
	<li>Board Governance</li>
	<li>Establishment of State Required Committees</li>
	<li>Implementation of State Approved Policy and Procedure Manuals</li>
	<li>Front Office-Back Office Support</li>
	<li>Payroll Services</li>
	<li>Medical Coding and Billing</li>
	<li>Ongoing Management Support</li>
</ul>";s:10:"post_title";s:23:"Turnkey Solutions	    	";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:17:"turnkey-solutions";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2014-09-18 12:02:37";s:17:"post_modified_gmt";s:19:"2014-09-18 12:02:37";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:34:"http://kannan.remco.dev/?page_id=8";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}s:8:"col_info";a:23:{i:0;O:8:"stdClass":13:{s:4:"name";s:2:"ID";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:2;s:8:"not_null";i:1;s:11:"primary_key";i:1;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:1;s:4:"blob";i:0;s:4:"type";s:3:"int";s:8:"unsigned";i:1;s:8:"zerofill";i:0;}i:1;O:8:"stdClass":13:{s:4:"name";s:11:"post_author";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:1;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:1;s:10:"unique_key";i:0;s:7:"numeric";i:1;s:4:"blob";i:0;s:4:"type";s:3:"int";s:8:"unsigned";i:1;s:8:"zerofill";i:0;}i:2;O:8:"stdClass":13:{s:4:"name";s:9:"post_date";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:19;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:8:"datetime";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:3;O:8:"stdClass":13:{s:4:"name";s:13:"post_date_gmt";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:19;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:8:"datetime";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:4;O:8:"stdClass":13:{s:4:"name";s:12:"post_content";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:2303;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:1;s:4:"type";s:4:"blob";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:5;O:8:"stdClass":13:{s:4:"name";s:10:"post_title";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:32;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:1;s:4:"type";s:4:"blob";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:6;O:8:"stdClass":13:{s:4:"name";s:12:"post_excerpt";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:0;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:1;s:4:"type";s:4:"blob";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:7;O:8:"stdClass":13:{s:4:"name";s:11:"post_status";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:7;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:8;O:8:"stdClass":13:{s:4:"name";s:14:"comment_status";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:6;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:9;O:8:"stdClass":13:{s:4:"name";s:11:"ping_status";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:6;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:10;O:8:"stdClass":13:{s:4:"name";s:13:"post_password";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:0;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:11;O:8:"stdClass":13:{s:4:"name";s:9:"post_name";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:30;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:1;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:12;O:8:"stdClass":13:{s:4:"name";s:7:"to_ping";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:0;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:1;s:4:"type";s:4:"blob";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:13;O:8:"stdClass":13:{s:4:"name";s:6:"pinged";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:0;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:1;s:4:"type";s:4:"blob";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:14;O:8:"stdClass":13:{s:4:"name";s:13:"post_modified";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:19;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:8:"datetime";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:15;O:8:"stdClass":13:{s:4:"name";s:17:"post_modified_gmt";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:19;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:8:"datetime";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:16;O:8:"stdClass":13:{s:4:"name";s:21:"post_content_filtered";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:0;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:1;s:4:"type";s:4:"blob";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:17;O:8:"stdClass":13:{s:4:"name";s:11:"post_parent";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:1;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:1;s:10:"unique_key";i:0;s:7:"numeric";i:1;s:4:"blob";i:0;s:4:"type";s:3:"int";s:8:"unsigned";i:1;s:8:"zerofill";i:0;}i:18;O:8:"stdClass":13:{s:4:"name";s:4:"guid";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:35;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:19;O:8:"stdClass":13:{s:4:"name";s:10:"menu_order";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:1;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:1;s:4:"blob";i:0;s:4:"type";s:3:"int";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:20;O:8:"stdClass":13:{s:4:"name";s:9:"post_type";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:4;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:1;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:21;O:8:"stdClass":13:{s:4:"name";s:14:"post_mime_type";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:0;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:0;s:4:"blob";i:0;s:4:"type";s:6:"string";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}i:22;O:8:"stdClass":13:{s:4:"name";s:13:"comment_count";s:5:"table";s:8:"wp_posts";s:3:"def";s:0:"";s:10:"max_length";i:1;s:8:"not_null";i:1;s:11:"primary_key";i:0;s:12:"multiple_key";i:0;s:10:"unique_key";i:0;s:7:"numeric";i:1;s:4:"blob";i:0;s:4:"type";s:3:"int";s:8:"unsigned";i:0;s:8:"zerofill";i:0;}}s:8:"num_rows";i:6;s:10:"return_val";i:6;}