<?php
/**
 * The Template Part for displaying the Preheader Theme Area.
 * 
 * The preheader is a collapsible, widget-ready theme area above the header.
 *
 * For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package     G1_Framework
 * @subpackage  G1_Theme01
 * @since       G1_Theme01 1.0.0
 */

// Prevent direct script access
if ( !defined('ABSPATH') )
    die ( 'No direct script access allowed' );
?>
<?php 
	$g1_mapping = array(
		'1/1'	=> 'g1-max',
		'1/2'	=> 'g1-one-half',
		'1/3'	=> 'g1-one-third',
		'1/4'	=> 'g1-one-fourth',
		'3/4'	=> 'g1-three-fourth',
	);

	$g1_composition = g1_get_theme_option( 'ta_preheader', 'composition', '1/3+1/3+1/3' );
    $g1_composition = 'none' === $g1_composition ? '' : $g1_composition;

	$g1_rows = strlen( $g1_composition ) ? explode( '_', $g1_composition ) : array();
    $g1_index = 1;
?>
<?php
    /* Executes a custom hook.
     * If you want to add some content before the g1-preheader,
     * hook into 'g1_preheader_before' action.
     */
    do_action( 'g1_preheader_before' );
?>

		
	
	<?php 
		/* Executes a custom hook.
		 * If you want to add some content after the g1-preheader,
		 * hook into 'g1_preheader_after' action.
		 */	
		do_action( 'g1_preheader_after' );
	?>
