<?php
/**
 * The Template Part for displaying the precontent.
 *
 * For the full license information, please view the Licensing folder
 * that was distributed with this source code.
 *
 * @package G1_Framework
 * @subpackage G1_Theme03
 * @since G1_Theme03 1.0.0
 */

// Prevent direct script access
if ( !defined('ABSPATH') )
    die ( 'No direct script access allowed' );
?>
<?php
    ob_start();
    do_action( 'g1_precontent' );
    $g1_precontent = trim( ob_get_clean() );
?>

<!-- BEGIN #g1-precontent -->
    <div id="g1-precontent">
	<?php
	$id = get_the_ID();
        $url = wp_get_attachment_url( get_post_thumbnail_id($post_id));
        $permalink = get_permalink( $id, $leavename ); 
        $list = get_post_ancestors( $post );
	if ($list[0] == 20): /*Staff page list menu*/
	    echo '<div class="full_width"><div class="g1-layout-inner menu_top_bg">';
	    echo do_shortcode('[widget id="nav_menu-3"]');
	    echo '</div></div>';
	    endif; /* End Staff page list menu*/
	
	if ($list[0] == 30) /*Parent Staff page list menu*/
	    {
		echo '<div class="full_width"><div class="g1-layout-inner menu_top_bg">';
		echo do_shortcode('[widget id="nav_menu-3"]');
		echo '</div></div>';
		echo '<div class="back_img" id="back_img">';
	    if($id == 32):
		if($url != null):
		    echo '<div class="feature_img">';
		    echo '<img src="'.$url.'">';
		    echo '</div>';		    
		else:	    
		    echo '<a href="'.$permalink.'"><div class="clent1"></div></a>';
		endif;
	    else:
		echo '<a href="'.site_url().'/?page_id=32"><div class="clent1"><img src="'.wp_get_attachment_url( get_post_thumbnail_id(32)).'"></div></a>';
	    endif;
	    if($id == 34):
		if($url != null):
		    echo '<div class="feature_img">';
		    echo '<img src="'.$url.'">';
		    echo '</div>';		    
		else:	    
		    echo '<a href="'.$permalink.'"><div class="clent1"></div></a>';
		endif;
	    else:
		echo '<a href="'.site_url().'/?page_id=34"><div class="clent1"><img src="'.wp_get_attachment_url( get_post_thumbnail_id(34)).'"></div></a>';
	    endif;
	    if($id == 36):
		if($url != null):
		    echo '<div class="feature_img">';
		    echo '<img src="'.$url.'">';
		    echo '</div>';		    
		else:	    
		    echo '<a href="'.$permalink.'"><div class="clent1"></div></a>';
		endif;
	    else:
		echo '<a href="'.site_url().'/?page_id=36"><div class="clent1"><img src="'.wp_get_attachment_url( get_post_thumbnail_id(36)).'"></div></a>';
	    endif;
	    if($id == 38):
		if($url != null):
		    echo '<div class="feature_img">';
		    echo '<img src="'.$url.'">';
		    echo '</div>';		    
		else:	    
		    echo '<a href="'.$permalink.'"><div class="clent1"></div></a>';
		endif;
	    else:
		echo '<a href="'.site_url().'/?page_id=38"><div class="clent1"><img src="'.wp_get_attachment_url( get_post_thumbnail_id(38)).'"></div></a>';
	    endif;
	    if($id == 40):
		if($url != null):
		    echo '<div class="feature_img">';
		    echo '<img src="'.$url.'">';
		    echo '</div>';		    
		else:	    
		    echo '<a href="'.$permalink.'"><div class="clent1"></div></a>';
		endif;
	    else:
		echo '<a href="'.site_url().'/?page_id=40"><div class="clent1"><img src="'.wp_get_attachment_url( get_post_thumbnail_id(40)).'"></div></a>';
	    endif;
	    if($id == 157):
		if($url != null):
		    echo '<div class="feature_img">';
		    echo '<img src="'.$url.'">';
		    echo '</div>';		    
		else:	    
		    echo '<a href="'.$permalink.'"><div class="clent1"></div></a>';
		endif;
	    else:
		echo '<a href="'.site_url().'/?page_id=157"><div class="clent1"><img src="'.wp_get_attachment_url( get_post_thumbnail_id(157)).'"></div></a>';
	    endif; /*End Parent Staff page list menu*/
	echo '</div>';       
	echo '<div class="full_width full_res"><div class="g1-layout-inner menu_top_bg">';
	echo do_shortcode('[widget id="nav_menu-4"]');
	echo '</div></div>';
 }
   ?> 	 
    <?php echo $g1_precontent; ?>
    <?php
     if(is_front_page()): /*Home page slider*/
	if(!preg_match('/(iPhone|iPod|iPad|Android|BlackBerry)/i', $_SERVER['HTTP_USER_AGENT'])): /*Slider hide mobile and ipad devices*/
    	?>
	     <!-- <?php //get_template_part( 'template-parts/g1_background', 'precontent' ); ?> -->
    <script type="text/javascript" src="/wp-content/themes/remco/hslider/js/jssor.core.js"></script>
    <script type="text/javascript" src="/wp-content/themes/remco/hslider/js/jssor.utils.js"></script>
    <script type="text/javascript" src="/wp-content/themes/remco/hslider/js/jssor.slider.js"></script>
    <script type="text/javascript" src="/wp-content/themes/remco/hslider/slider.js"></script>

    <div id="homepage_slider" class="homepage_slider">
	<!-- Jssor Slider Begin -->
	<!-- You can move inline styles to css file or css block. -->

	<div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 810px; height: 220px; background: #000; overflow: hidden; display: none;">
    
	    <!-- Loading Screen -->
	    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
		<div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block; background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
		</div>
		<div style="position: absolute; display: block; background: url(/wp-content/themes/remco/hslider/img/loading.gif) no-repeat center center;
		    top: 0px; left: 0px;width: 100%;height:100%;">
		</div>
	    </div>
    
	    <!-- Slides Container -->
	    <div u="slides" style="cursor: move; position: absolute; left: 250px; top: 0px; width:560px; height: 220px;
		overflow: hidden;">
		<div>
		    <img u="image" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(108) )?>" />
		    <div u="thumb">
			<a href="<?php echo get_permalink(108); ?>"><div class="t tthumb_1"><?php echo get_post_meta(108, 'Slider_Left_content', true); ?> </div></a>
		    </div>
		</div>
		<div>
		    <img u="image" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(112) )?>" />
		    <div u="thumb">
			<a href="<?php echo get_permalink(112); ?>"><div class="t tthumb_2"><?php echo get_post_meta(112, 'Slider_Left_content', true); ?></div></a>
		    </div>
		</div>
		<div>
		    <img u="image" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(8) )?>" />
		    <div u="thumb">
			<a href="<?php echo get_permalink(8); ?>"><div class="t tthumb_3"><?php echo get_post_meta(8, 'Slider_Left_content', true); ?></div></a>
		    </div>
		</div>
		<div>
		    <img u="image" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(116) )?>" />
		    <div u="thumb">
			<a href="<?php echo get_permalink(116); ?>"><div class="t tthumb_4"><?php echo get_post_meta(116, 'Slider_Left_content', true); ?></div></a>
		    </div>
		</div>
		
	    </div>
	    
	    <!-- ThumbnailNavigator Skin Begin -->
	    <div u="thumbnavigator" class="jssort11 jssortthumb_container" style="position: absolute; width: 250px; height: 220px; left:0px; top:0px;">
		<!-- Thumbnail Item Skin Begin -->
		        <link type="text/css" href="/wp-content/themes/remco/hslider/slider.css" rel="stylesheet" />
		<div u="slides" style="cursor: move;">
		    <div u="prototype" class="p jssortthumb_container_single" style="position: absolute; width: 250px; height: 55px; top: 0; left: 0;">
			<!-- <div u="prototype" class="p jssortthumb_container_single" style="position: absolute; width: 285px; height: 55px; top: 0; left: 0;"> -->
			<thumbnailtemplate style=" width: 100%; height: 100%; border: none;position:absolute; top: 0;  left: 0;"></thumbnailtemplate>
		    </div>
		</div>
		<!-- Thumbnail Item Skin End -->
	    </div>
	    <!-- ThumbnailNavigator Skin End -->
	
	</div>

	<!-- Jssor Slider End -->

    </div>
	<?php
	endif;/*End Slider hide mobile and ipad devices*/
        elseif($id == 20): /* Staff Menu display why-remco page */
		    echo '<div class="full_width"><div class="g1-layout-inner menu_top_bg">';
		    echo do_shortcode('[widget id="nav_menu-3"]');
		    echo '</div></div>';
		if($url != null):
		    echo '<div class="page_img">';
		    echo '<img src="'.$url.'" style="height:360px;width:100%;border-bottom:3px solid #000;">';
		    echo '</div>';
		endif; /* End Staff Menu display why-remco page */
	elseif(!$list[0] == 30): /*page image desiplay code*/
	        if($url != null):
		    echo '<div class="page_img">';
		    echo '<img src="'.$url.'" style="height:360px;width:100%;border-bottom:3px solid #000;">';
		    echo '</div>';
		endif;/*End page image desiplay code*/
	
	 endif; /* End */
     
	
	
	
    ?>

</div>
<!-- END #g1-precontent -->
